<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sistema Teste</title>

<link href="<?=base_url()?>assets/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
<link href="<?=base_url()?>assets/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/sistema_teste.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">



</head>

<body class="full-width page-condensed" id="imgFundo">
<?= $contents ?>
<script src="<?=base_url()?>assets/bootstrap/assets/js/jquery.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-transition.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-alert.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-modal.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-dropdown.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-scrollspy.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-tab.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-tooltip.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-popover.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-button.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-collapse.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-carousel.js"></script>
<script src="<?=base_url()?>assets/bootstrap/assets/js/bootstrap-typeahead.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sistema_teste.js"></script>
</body>
</html>