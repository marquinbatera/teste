<!--<img src="<?= base_url()?>assets/images/back.fw.png" />-->
<div class="container droppedHover">

      <form class="form-signin" action="<?= base_url()?>login/index" method="post" role="form">
        <h2 class="form-signin-heading"><img src="<?= base_url()?>/assets/images/logo.png" /></h2>
        <input type="text" class="input-block-level" placeholder="Login" name="login">
        <input type="password" class="input-block-level" placeholder="Senha" name="senha">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Lembre-me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Login</button>
      </form>

</div>