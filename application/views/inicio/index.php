<?php $this->load->helper('cookie'); ?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= base_url()?>inicio/index"><img src="<?= base_url()?>/assets/images/mini-logo.png" /></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Funcionários <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="<?= base_url()?>cliente/cadastrar">Cadastrar</a></li>
            <li><a class="menu_crm2" link="<?= base_url()?>cliente/consultar" href="<?= base_url()?>cliente/consultar">Consultar</a></li>
            <li class="divider"></li>
            <li><a href="javascript:">Relatório personalizado</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Pesquisar">
        </div>
        <button type="submit" class="btn btn-default">Iniciar</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="<?= base_url()?>index.php" role="button" aria-expanded="false">Sair</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span12">
        	<div class="span12" id="conteudo">
      
               	<div class="jumbotron" id="jumbotron-fundo1">
                <img class="text-center" src="<?= base_url()?>assets/images/logo-inicio.png" />
                  <h1>Farm Online</h1>
                  <p>Começou como um teste de contratação e hoje é meu meio de estudar. Aqui eu emprego várias tecnologias e práticas de programação tanto no back-end quanto no front-end, utilizando o framework CodeIgniter e Bootstrap com Ajax e Jquery.</p>
                  <p><a class="btn btn-danger btn-lg" href="<?= base_url()?>cliente/cadastrar">Cadastrar Funcionários</a></p>
                </div>
                
        	</div><!--/span-->	
        </div><!--/span-->
      </div><!--/row-->
</div><!--/.fluid-container-->				
			