<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content" id="mensagem">
      
    </div>
  </div>
</div>

	<div class="alert alert-dismissible alert-success alerta" id="alertCompra">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>Produto adicionado ao carrinho!</strong>
    </div>
    <div class="panel panel-danger">
     	<div class="panel-heading">Produtos</div>
      	<div class="panel-body">
 
            <form action="#" role="form" id="form">
                <div class="row-fluid">
                    <ul class="thumbnails">
                      <?php foreach($prod as $produto): ?>
                      
                      <li class="span3">
                        <div class="thumbnail">
                          <img src="<?= base_url()?>assets/images/<?php echo $produto->img; ?>" style=" width:200px; height:200px" alt="">
                          <h3 id="nome"><?php echo $produto->nome; ?></h3>
                          <h4 id="preco">Preço: <?php echo $produto->preco; ?></h4>
                          <input type="hidden" value="<?php echo $produto->codigo; ?>" name="codigo" />
                          <button id="btnModal" class="btn btn-success text-center" link="<?= base_url(); ?>cliente/modal" method="POST" codigo="<?php echo $produto->codigo; ?>" div="mensagem" data-toggle="modal" data-target="#myModal" class="btn btn-success" type="button"><i class="glyphicon glyphicon-shopping-cart"></i> Comprar</button>
                        </div>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                </div>
                
            </form>
    	</div>
      	</div>
	</div>