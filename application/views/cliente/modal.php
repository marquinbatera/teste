<?php /*?><?php foreach($modal as $m): ?><?php */?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Carrinho de compra</h4>
  </div>
  <div class="modal-body">
  	<img src="<?= base_url()?>assets/images/loader.gif" class="imgLoad" />
    <form  action="javascript:" class="form-horizontal" id="form">
      <fieldset>
        <legend><?php echo $modal[0]->nome; ?></legend>
        <div class="form-group">
        <label class="col-lg-2 control-label">Preço:</label>
        <div class="col-lg-4">
        	<input class="form-control" type="text" value="R$<?php echo $modal[0]->preco; ?>" />
        </div>
        </div>
        <div class="form-group">
          <label class="col-lg-2 control-label">Forma de Pagamento</label>
          <div class="col-lg-10">
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                Cartão de Crédito
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                A vista
              </label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="select" class="col-lg-2 control-label">Quantidade</label>
          <div class="col-lg-3">
            <select class="form-control" id="select" name="quantidade">
              <?php for($i=0;$i<$modal[0]->quantidade;$i++){?>
              <option><?php echo $i+1;?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="textArea" class="col-lg-2 control-label">informações adicionais</label>
          <div class="col-lg-10">
            <textarea class="form-control" rows="3" id="textArea"></textarea>
            <span class="help-block">Algo relevante sobre a compra do produto.</span>
          </div>
        </div>
      </fieldset>
      <input type="hidden" value="<?php echo $modal[0]->codigo; ?>" name="codigo" />
      <input type="hidden" value="<?php echo $modal[0]->quantidade; ?>" name="quantAtual" />
    </form>
  </div>
  <div class="modal-footer" id="divBotoes">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    <button type="button" id="btnEnviar" link="<?= base_url(); ?>cliente/atualiza" method="POST" form="form" div="aviso" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Adicionar</button>
  </div>
