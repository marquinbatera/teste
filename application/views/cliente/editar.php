<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Editar Funcionário</h4>
      </div>
      <div class="modal-body">
       	
        <form class="form-horizontal" method="post" action="<?= base_url();?>cliente/valida_cadastro_edit" id="editFuncionario" enctype="multipart/form-data">
          <fieldset> 
            <div class="form-group">
              <label for="inputNome" class="col-lg-2 control-label">Nome*</label>
              <div class="col-lg-10">
              	<input type="hidden" value="<?php echo $data[0]->codigo ?>" name="codigo" />
                <input type="text" class="form-control" id="inputNome" value="<?php echo $data[0]->nome ?>" name="nome" placeholder="Marcos Meira">
                <div class="alert alert-dismissible alert-warning" id="alertNome" style="display:none">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Preencha o nome do funcionário!</strong>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-lg-2 control-label">E-mail</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="inputEmail" value="<?php echo $data[0]->email ?>" name="email" placeholder="marquinbatera@gmail.com">
              </div>
            </div>
            <div class="form-group">
              <label for="inputSetor" class="col-lg-2 control-label">Setor*</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="inputSetor" value="<?php echo $data[0]->setor ?>" name="setor" placeholder="Produção">
                <div class="alert alert-dismissible alert-warning" id="alertSetor" style="display:none">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Preencha o setor do funcionário!</strong>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputCargo" class="col-lg-2 control-label">Cargo*</label>
              <div class="col-lg-10">
                <input type="text" class="form-control" id="inputCargo" value="<?php echo $data[0]->cargo ?>" name="cargo" placeholder="Desenvolvedor PHP">
                <div class="alert alert-dismissible alert-warning" id="alertCargo" style="display:none">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Preencha o Cargo do funcionário!</strong>
                </div>
              </div>
            </div>
            <div class="form-group">
          <label class="col-lg-2 control-label">Sexo</label>
          <div class="col-lg-10">
            <div class="radio">
              <label>
                <input type="radio" name="sexo" value="masculino" <?php echo ($data[0]->sexo == 'masculino' ? 'checked="checked"' : '' )?>>
                Masculino
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="sexo" value="feminino" <?php echo ($data[0]->sexo == 'feminino' ? 'checked="checked"' : '' )?>>
                Feminino
              </label>
            </div>
          </div>
        </div>
            <div class="form-group">
              <label for="select" class="col-lg-2 control-label">Imagem</label>
              <div class="col-lg-10">
                <input type="file" class="form-control" id="img" name="img">
              </div>
            </div>
          </fieldset>
         
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Fechar</button>
        	<button type="button" id="btnEditar" class="btn btn-primary btnEnviar"><i class="glyphicon glyphicon-ok"></i> Cadastrar</button>
        </form>
      </div>
    </div>
 