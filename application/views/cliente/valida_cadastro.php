<?= $this->load->view('partials/topo'); ?>
<div class="jumbotron">
  <h1><?php echo $this->session->userdata('mensagem'); ?> <i class="<?php echo $this->session->userdata('icon'); ?>"></i></h1>
  <p> 
    	<br />
        <a class="btn btn-primary menu_crm" link="<?= base_url()?>cliente/consultar"><i class="glyphicon glyphicon-search"></i> Consultar</a>
        <a class="btn btn-primary" href="<?= base_url()?>cliente/cadastrar"><i class="glyphicon glyphicon-user"></i> <?php echo $this->session->userdata('botao'); ?></a>
  </p>
</div>
<?= $this->load->view('partials/footer'); ?>