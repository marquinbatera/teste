<?= $this->load->view('partials/topo'); ?>

<div class="jumbotron" id="jumbotron-fundo1">
  <h1>Cadastro de Funcionários</h1>
  <p> 
    <form class="form-horizontal" method="post" action="<?= base_url();?>cliente/valida_cadastro" id="formFuncionario" enctype="multipart/form-data">
      <fieldset>
        <div class="form-group">
          <div class="col-lg-10 col-lg-offset-2">
            <a href="<?= base_url()?>inicio/index" class="btn btn-default"><i class="glyphicon glyphicon-remove"></i> Cancelar</a>
            <button type="button" id="btnUpload" class="btn btn-primary btnEnviar"><i class="glyphicon glyphicon-ok"></i> Cadastrar</button>
          </div>
        </div> 
        <div class="form-group">
          <label for="inputNome" class="col-lg-2 control-label">Nome*</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputNome" name="nome" placeholder="Marcos Meira">
            <div class="alert alert-dismissible alert-warning" id="alertNome" style="display:none">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>Preencha o nome do funcionário!</strong>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-lg-2 control-label">E-mail</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputEmail" name="email" placeholder="marquinbatera@gmail.com">
          </div>
        </div>
        <div class="form-group">
          <label for="inputSetor" class="col-lg-2 control-label">Setor*</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputSetor" name="setor" placeholder="Produção">
            <div class="alert alert-dismissible alert-warning" id="alertSetor" style="display:none">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>Preencha o setor do funcionário!</strong>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="inputCargo" class="col-lg-2 control-label">Cargo*</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="inputCargo" name="cargo" placeholder="Desenvolvedor PHP">
            <div class="alert alert-dismissible alert-warning" id="alertCargo" style="display:none">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>Preencha o Cargo do funcionário!</strong>
            </div>
          </div>
        </div>
        <div class="form-group">
      <label class="col-lg-2 control-label">Sexo</label>
      <div class="col-lg-10">
        <div class="radio">
          <label>
            <input type="radio" name="sexo" value="masculino" checked="">
            Masculino
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="sexo" value="feminino">
            Feminino
          </label>
        </div>
      </div>
    </div>
        <div class="form-group">
          <label for="select" class="col-lg-2 control-label">Imagem</label>
          <div class="col-lg-10">
            <input type="file" class="form-control" id="img" name="img">
            <div class="alert alert-dismissible alert-warning" id="alertImagem" style="display:none">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>Fala upload de uma foto 'JPEG' para funcionário!</strong>
            </div>
          </div>
        </div>
      </fieldset>
    </form>
  </p>
</div>
<?= $this->load->view('partials/footer'); ?>