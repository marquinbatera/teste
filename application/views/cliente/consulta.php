<?php echo $this->load->view('partials/topo');?>
<div class="modal" id="editModal">
  <div class="modal-dialog" id="conteudoModal">
    
  </div>
</div>

<div class="jumbotron" id="jumbotron">
  <h1>Consultar</h1>
  <p>
  <fieldset>
     <div class="form-group col-lg-12">
     	<div class="input-group col-lg-3">
        <input type="text" id="busca" class="form-control" placeholder="Busca Geral Jquery">
        <span class="input-group-btn">
          <button id="teste" pesquisa="total" campo="nome" class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
        </span>
      </div>
     </div>
    <div class="form-group col-lg-12">
      <div class="input-group col-lg-3">
        <input type="text" id="nomePesquisa" class="form-control" placeholder="Nome">
        <span class="input-group-btn">
          <button id="pesquisar" pesquisa="nomePesquisa" campo="nome" class="btn btn-default pesquisar" type="button"><i class="glyphicon glyphicon-search"></i></button>
        </span>
      </div>
      <div class="input-group col-lg-3">
        <input type="text" id="setorPesquisa" class="form-control" placeholder="Setor">
        <span class="input-group-btn">
          <button id="pesquisar" pesquisa="setorPesquisa" campo="setor" class="btn btn-default pesquisar" type="button"><i class="glyphicon glyphicon-search"></i></button>
        </span>
      </div>
      <div class="input-group col-lg-3">
        <input type="text" id="cargoPesquisa" class="form-control" placeholder="Cargo">
        <span class="input-group-btn">
          <button id="pesquisar" pesquisa="cargoPesquisa" campo="cargo" class="btn btn-default pesquisar" type="button"><i class="glyphicon glyphicon-search"></i></button>
        </span>
      </div>
    </div>
  </fieldset>
  </p>
  <p>
  <table class="table table-striped">
    	<thead>
        <tr class="danger text-center">
        	<td>Código</td><td>Nome</td><td>E-mail</td><td>Setor</td><td>Cargo</td><td>Sexo</td><td>Imagem</td><td colspan="2">Editar</td>
        </tr>
        </thead>
        <tbody id="consultaProdutos">
        	
        
        </tbody>
  </table>
    
  </p>
</div>
<?php echo $this->load->view('partials/footer');?>