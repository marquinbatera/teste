<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -  
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in 
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see http://codeigniter.com/user_guide/general/urls.html
	**/

	var $menus;
	
	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$login = $this->input->post('login');
		$senha = $this->input->post('senha');
		
		$dados = array('login' => $login, 'rand' => rand(1,9999999));
		$this->session->set_userdata($dados);
		
		$this->template->load('templates/template_inicio', 'inicio/Index');
		
	}
}