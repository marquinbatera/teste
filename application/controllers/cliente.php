<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente extends CI_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -  
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in 
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see http://codeigniter.com/user_guide/general/urls.html
	**/
	
	public function __construct(){
		parent::__construct();
	}

	public function cadastrar()
	{

		
		$this->template->load('templates/template_inicio', 'cliente/cadastrar');
		
	}
	
	public function editar()
	{
		$codigo = $this->input->post('codigo');
		
		$this->load->model('model_inicio', 'model_inicio');
		$result['data'] = $this->model_inicio->editaFunc($codigo);
		$this->load->view('cliente/editar', $result);
		
	}
	
	public function upload($destino, $nome, $largura, $pasta)
	{
		$img = imagecreatefromjpeg($destino);
		$x = imagesx($img);
		$y = imagesy($img);
		$altura = ($largura * $y) / $x;
		
		$novaImagem = imagecreatetruecolor($largura, $altura);
		imagecopyresampled($novaImagem, $img, 0, 0, 0, 0, $largura, $altura, $x, $y);
		imagejpeg($novaImagem, "$pasta/$nome");
		
		imagedestroy($img);
		imagedestroy($novaImagem);
	}
	
	public function consultar()
	{
		$this->load->model('model_inicio', 'model_inicio');
		$data['prod'] = $this->model_inicio->buscaDados();
		//$this->load->view('cliente/consulta', $data);
		$this->template->load('templates/template_inicio', 'cliente/consulta');
	}
	
	public function grid()
	{
		
		$this->load->model('model_inicio', 'model_inicio');
		
		$nome = $this->input->post('nome');
		$campo = $this->input->post('campo');
		
		if(empty($nome) || empty($campo)){
			$data['prod'] = $this->model_inicio->buscaDados();
		}else{
			$data['prod'] = $this->model_inicio->consultaDados($nome, $campo);
		}
		$this->load->view('cliente/grid', $data);
	}
	
	public function valida_cadastro()
	{
		if($this->input->post()){
			
				$pasta = './assets/images';
				$permite = array('image/jpg','image/jpeg','image/pjpeg');
				
				$imagem = $_FILES['img'];
				$destino = $imagem['tmp_name'];
				$nome = $imagem['name'];
				$tipo = $imagem['type'];
				
				
				if(!empty($nome) && in_array($tipo, $permite)){
					
					$data = array(
						'nome' => $this->input->post('nome'),
						'email' => $this->input->post('email'),
						'setor' => $this->input->post('setor'),
						'cargo' => $this->input->post('cargo'),
						'sexo' => $this->input->post('sexo'),
						'img' => $nome
					);
					$this->db->insert('funcionario', $data);
					
					$this->upload($destino,$nome, 600, $pasta);
					$resultado = array('mensagem' => 'Cadastro Realizado com sucesso', 'icon' => 'gryphicon gryphicon-success', 'botao' => 'Novo Cadastro');
					$this->session->set_userdata($resultado);
				}else{
					$resultado = array('mensagem' => 'Aceitamos apenas imagens JPEG', 'icon' => 'glyphicon glyphicon-remove', 'botao' => 'Tentar Novamente');
					$this->session->set_userdata($resultado);
				}
			
		}
		$this->template->load('templates/template_inicio', 'cliente/valida_cadastro');
	}
	
	public function valida_cadastro_edit()
	{
		if($this->input->post()){
				
				$codigo = $this->input->post('codigo');
				
				// se tiver upload executa a validação de upload
				if($this->input->post('img')){
					$pasta = './assets/images';
					$permite = array('image/jpg','image/jpeg','image/pjpeg');
					
					$imagem = $_FILES['img'];
					$destino = $imagem['tmp_name'];
					$nome = $imagem['name'];
					$tipo = $imagem['type'];
					
					if(!empty($nome) && in_array($tipo, $permite)){
						$data = array(
							'nome' => $this->input->post('nome'),
							'email' => $this->input->post('email'),
							'setor' => $this->input->post('setor'),
							'cargo' => $this->input->post('cargo'),
							'sexo' => $this->input->post('sexo'),
							'img' => $nome
						);
						
						$this->db->where('codigo',$codigo);
						$this->db->update('funcionario', $data);
						$this->upload($destino,$nome, 600, $pasta);
						
						$resultado = array('mensagem' => 'Edição realizado com sucesso', 'icon' => 'gryphicon gryphicon-success', 'botao' => 'Novo Cadastro');
						$this->session->set_userdata($resultado);
						
					}else{
						$resultado = array('mensagem' => 'Aceitamos apenas imagens JPEG', 'icon' => 'gryphicon gryphicon-remove', 'botao' => 'Novo Cadastro');
						$this->session->set_userdata($resultado);
					}
				}else{
				// fim validação upload
				
					$data = array(
						'nome' => $this->input->post('nome'),
						'email' => $this->input->post('email'),
						'setor' => $this->input->post('setor'),
						'cargo' => $this->input->post('cargo'),
						'sexo' => $this->input->post('sexo')
					);
					
					$this->db->where('codigo',$codigo);
					$this->db->update('funcionario', $data);
					
					$resultado = array('mensagem' => 'Edição realizado com sucesso', 'icon' => 'gryphicon gryphicon-success', 'botao' => 'Novo Cadastro');
					$this->session->set_userdata($resultado);
				}
			
		}
		$this->template->load('templates/template_inicio', 'cliente/valida_cadastro_edit');
	}
	
	public function produtos()
	{
		$this->load->model('model_inicio', 'model_inicio');
		$data['prod'] = $this->model_inicio->buscaDados();
		$this->load->view('cliente/produtos', $data);
	}
	public function modal()
	{
		$codigo = $_POST['codigo'];
		$this->load->model('model_inicio', 'model_inicio');
		$data['modal'] = $this->model_inicio->modal($codigo);
		$this->load->view('cliente/modal', $data);
	}
	
	public function atualiza()
	{
		$codigo = $_POST['codigo'];
		$quantidade = $_POST['quantidade'];
		$atual = $_POST['quantAtual'];
		$login = $this->session->userdata('login');
		$rand = $this->session->userdata('rand');
		
		
		$dadosCarrinho = array(
			'codigoProduto' => $codigo,
			'quantidadeProduto' => $quantidade,
			'usuario' => $login,
			'numberRand' => $rand
		);
		// verifica se o produto já existe no carrinho, caso sim ele apenas atualiza a quantidade.
		$this->load->model('model_inicio', 'model_inicio');
		$carrinho = $this->model_inicio->get_carrinho($codigo, $rand);
		
		if(!empty($carrinho)){
			if($codigo == $carrinho[0]->codigoProduto && $rand == $carrinho[0]->numberRand){
				
				$quant = $carrinho[0]->quantidadeProduto + $quantidade;
				$atualQuant = array(
					'quantidadeProduto' => $quant
				);
				
				$this->db->where('codigoProduto',$codigo);
				$this->db->update('carrinho', $atualQuant);
			}
		}else{
			$this->db->insert('carrinho', $dadosCarrinho);
		}
		
		// fim verifica produto já existe

	}
	public function carrinho()
	{
		$this->load->model('model_inicio', 'model_inicio');
		$produtos = $this->model_inicio->get_produtos();
		
		echo json_encode($produtos);
	}
	public function removeFuncionario()
	{
		$codigo = $this->input->post('codigo');

		$this->db->where('codigo',$codigo);
		$this->db->delete('funcionario');
	}
	
	public function logout()
	{
   		redirect('login/index', 'reflash');
	}
}