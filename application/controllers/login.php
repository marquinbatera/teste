<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -  
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in 
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see http://codeigniter.com/user_guide/general/urls.html
	**/

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->load->helper('cookie');
		if($this->input->post()){
			$login = $this->input->post('login');
			$senha = md5($this->input->post('senha'));
			
			$this->load->model('model_inicio', 'model_inicio');
			$result = $this->model_inicio->getLogin($login, $senha);
			
			if(!$result){
				echo '<script>alert("Login ou senha incorretos"); </script>';
			}else{
				redirect('inicio/index', 'reflash');
			}
			
			$dados = array('login' => $login, 'rand' => rand(1,9999999));
			$this->session->set_userdata($dados);
		}
		
		
		$this->template->load('templates/template_login', 'login/Index');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */