function ajax_reload(botao){
	var url = botao[0].attributes[2].value;
	var method = botao[0].attributes[3].value;
	var form = botao[0].attributes[4].value;
	var div = botao[0].attributes[5].value;
	
	$.ajax({
		type: method,
		url: url,
		data: $('#'+form).serialize(),
		async:true,
		success: function(data){
			$('#myModal').modal('hide');
			$('#'+div).fadeIn(500).delay(2000).fadeOut(500);
			if(div != ''){
				$('#'+div).html(data);
			}else{
				alert(data);
			}
		}	
	})
	
}

function valida_cadastro(){
	$.ajax({
		type:'POST',
		url:'../cliente/valida_cadastro',
		success: function(data){
			var conteudo = JSON.parse(data);
			$('#conteudo').html('');
			var mensagem = value.mensagem;
			var icon = value.icon;
			$('#conteudo').append('<div class="jumbotron" id="jumbotron-fundo1"><h1>'+mensagem+'<i class="'+icon+'"></i></h1><p>teste!!!</p></div>');
		}
	})
}

function carrinho(){
	$.ajax({
		type:'POST',
		url:'../cliente/carrinho',
		success: function(data){
			var conteudo = JSON.parse(data);
			$('#carrinho').html('');
			var precoTotal = 0.00;
			$.each( conteudo, function(index, value){
				var codigo = value.codCarrinho;
				var imagem = value.img;
				var nome = value.nome;
				var quantidade = value.quantidadeProduto;
				var preco = parseFloat(value.preco).toFixed(2);
				var precoFinal = (preco * quantidade).toFixed(2);
				precoTotal += parseFloat(precoFinal);
				$('#carrinho').append('<tr class="linha'+codigo+'"><td><img style="width:100px;" class="img-circle" src="../assets/images/'+imagem+'"</td><td>'+nome+'</td><td>'+quantidade+'</td><td>R$'+preco+'</td><td>R$'+precoFinal+'</td><td><button type="button" id="removeProduto" cod="'+codigo+'" class="close" style="color:#C81616;">×</button></td></tr>');
				});
				$('#carrinho').append('<tr class="dafault"><td>Total:</td><td></td><td></td><td></td><td>R$'+precoTotal.toFixed(2)+'</td><td><button type="button" id="btnComprar" class="btn btn-success">Finalizar</button></td></tr>');
		}	
	})
}

function consultProdutos(id, campo){
	var nome = $('#'+id).val();
	$.ajax({
		type:'POST',
		url:'../cliente/grid',
		data:{
			nome:nome,
			campo:campo
		},
		success: function(data){
			$('#consultaProdutos').html(data);
		}	
	})
}

function ajax_carrega(botao){
	var url = botao[0].attributes[2].value;
	var method = botao[0].attributes[3].value;
	var codigo = parseInt(botao[0].attributes[4].value);
	var div = botao[0].attributes[5].value;
	
	$.ajax({
		type: method,
		url: url,
		data: {
			codigo:codigo
		},
		async:true,
		success: function(data){
			if(div != ''){
				$('#'+div).html(data);
			}else{
				alert(data);
			}
			$('#alerta').hide();
		}	
	})
}



$(document).ready(function(){
	
	consultProdutos();
	
	$(document).on('click', '.menu_crm', function(){
		var link = $(this).attr('link');
		var classe = $(this).attr('class');
		
		if(classe != 'expand level-opened' && classe != 'expand level-closed'){
			$('#conteudo').html('<center><img src="../assets/images/loader.gif"></center>');
			$.ajax({
				type:'POST',
				url: link,
				success: function(data){
					$('#conteudo').html(data);
					var obj = {id: 'idPagina'};
					history.pushState (obj, "", link);
					consultProdutos();
				}	
				
			})
		}	
	});
	
	$(document).on('click', '#teste', function(){
		var val = $('#busca').val();
		if(val != ''){
			$('.controle').hide();
		}
		$(".controle:contains('"+val+"')").show();
	});
	
	$(document).on('click', '#btnEnviar', function(){
		$('#divBotoes').hide();
		$('.imgLoad').show('slow');
		console.log($(this));
		ajax_reload($(this));
	});
	
	$(document).on('click', '#menuCarrinho', function(){
		carrinho();
	});
	
	$(document).on('click', '.pesquisar', function(){
		var id = $(this).attr('pesquisa');
		var campo = $(this).attr('campo');
		console.log($(this));
		consultProdutos(id, campo);
	});
	
	$(document).on('click', '#editFunc', function(){
		var codigo = $(this).attr('cod');
		$.ajax({
			type:'POST',
			url:"../cliente/editar",
			data:{
				codigo:codigo
			},
			success: function(data){
				$('#conteudoModal').html(data);
			}
		});
	});
	
	$(document).on('click', '#removeFuncionario', function(){
		var codigo = $(this).attr('cod');
		$.ajax({
			type:'POST',
			url:"../cliente/removeFuncionario",
			data:{
				codigo:codigo
			},
			success: function(data){
				consultProdutos();
			}
		});
	});
	
	$(document).on('click', '#btnModal', function(){
		console.log($(this));
		ajax_carrega($(this));
	});
	
	$(document).on('click', '#btnUpload', function(){
		var nome = $('#inputNome').val();
		var setor = $('#inputSetor').val();
		var cargo = $('#inputCargo').val();
		var img = $('#img').val();
		
		if(nome == ''){
			$('#alertNome').fadeIn(500).delay(2000).fadeOut(500);
			}else if(setor == ''){
				$('#alertSetor').fadeIn(500).delay(2000).fadeOut(500);
			}else if(cargo == ''){
				$('#alertCargo').fadeIn(500).delay(2000).fadeOut(500);
			}else if(img == ''){
				$('#alertImagem').fadeIn(500).delay(2000).fadeOut(500);
			}else{
				$('#formFuncionario').submit();
		};
		
	});
	
	$(document).on('click', '#btnEditar', function(){
		var nome = $('#inputNome').val();
		var setor = $('#inputSetor').val();
		var cargo = $('#inputCargo').val();
		var img = $('#img').val();
		
		if(nome == ''){
			$('#alertNome').fadeIn(500).delay(2000).fadeOut(500);
			}else if(setor == ''){
				$('#alertSetor').fadeIn(500).delay(2000).fadeOut(500);
			}else if(cargo == ''){
				$('#alertCargo').fadeIn(500).delay(2000).fadeOut(500);
			}else{
				$('#editFuncionario').submit();
		};
		
	});
	
});